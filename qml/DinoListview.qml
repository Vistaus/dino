import QtQuick 2.4
import QtQuick.Window 2.2
import Ubuntu.Components 1.3
import Ubuntu.Components.ListItems 1.3 as ListItem
import "components"


Page {
    id: dinoListViewPage
    visible: false
    header: mainHeader

    Flickable {
        id: dinoContainer
        width: parent.width; height: parent.height
        anchors.top: dinoListViewPage.header.bottom
        anchors.bottom: parent.bottom

        Component {
            id: dinoDelegate

                Column {
                    width: parent.width

                    /*
                    ListItem.Standard{
                        text: collection[index].name
                        iconName: ""
                        iconSource:"images/"+imgFolder+"/thumbnails/tn_img"+ collection[index].id+".png"
                        MouseArea {
                             anchors.fill: parent
                             onClicked: {
                                 dinoList.currentIndex = index
                                 selectedIndex = dinoList.currentIndex
                                 items = dinoList.count - 1
                                 console.log(items)
                                 headerTitle = collection[index].name
                                 expanded = false
                                 pageStack.push(page2)
                             }
                        }
                        progression: true
                    }
                    */
                    CustomListItem{
                        //text
                        name: collection[index].name
                        //icon
                        iconSource: "../images/"+imgFolder+"/thumbnails/tn_img"+ collection[index].id+".png"
                        iconWidth: density <= 10 ? 36 : 96
                        iconHeight: iconWidth
                        progressSymbol: true

                        MouseArea {
                             anchors.fill: parent
                             onClicked: {
                                 dinoList.currentIndex = index
                                 selectedIndex = dinoList.currentIndex
                                 items = dinoList.count - 1
                                 console.log(items)
                                 headerTitle = collection[index].name
                                 expanded = false
                                 pageStack.push(page2)
                             }
                        }

                    }

                }

        }

        ListView {
            id:dinoList
            anchors.fill: parent
            model: collection
            delegate: dinoDelegate
            //highlight: Rectangle { color: "lightsteelblue"; radius: 5 }
            //highlight: highlight
            focus: true
            highlightFollowsCurrentItem : true

        }

    }

}
