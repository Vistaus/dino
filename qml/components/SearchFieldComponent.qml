import QtQuick 2.4
import Ubuntu.Components 1.3
import "../dino.js" as DinosaursData

Component {
    id: searchFieldComponent
    TextField {
        inputMethodHints: Qt.ImhNoPredictiveText
        placeholderText: i18n.tr("Search Dinosaur")
        onTextChanged: {
            if (text.length > 0) {

                collection = DinosaursData.dinosaurs
                var entry = text.replace(/\b\w/g, function(l){ return l.toUpperCase() })

                function search(arr, s, name){
                    var matches = [], i, key;

                    for(i = arr.length; i--; )
                        for(key in arr[i] )
                            if(key === name)
                              if( arr[i][key].indexOf(s) > -1 )
                                  matches.unshift( arr[i] ); //push at the beginning of the "matches" array

                    return matches;
                };

                collection = search(collection, entry, 'name');

            } else {

                collection = DinosaursData.dinosaurs

            }
        }
    }
}
