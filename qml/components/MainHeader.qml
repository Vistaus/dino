import QtQuick 2.4
import Ubuntu.Components 1.3

PageHeader {
    id: mainHeader

    title: i18n.tr(headerTitle)
    StyleHints {
        foregroundColor: "#FFFFFF"
        backgroundColor: "#1976D2"
        dividerColor: UbuntuColors.slate
    }

    leadingActionBar.actions: [
        Action {
            visible: false
        }
    ]
    trailingActionBar.actions: [
        Action {
            iconName: "filter"
            text: ""
            onTriggered: {
                expanded = true;
            }
        },
        Action {
            iconName: "search"
            text: ""
            onTriggered: {
              page1.header = searchHeader
              searchHeader.field.item.forceActiveFocus() //force focus on the search field
            }
        },
        Action {
            iconName: "info"
            text: ""
            onTriggered: {
                headerTitle = i18n.tr("About")
                pageStack.push(page3)
            }
        }
    ]

    //trailingActionBar.numberOfSlots: 2
    visible: true
}
