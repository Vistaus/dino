import QtQuick 2.4
import QtQuick.Window 2.2

import Ubuntu.Components 1.3
import Ubuntu.Components.ListItems 1.3 as ListItem

import "../dino.js" as DinosaursData

Rectangle {
    id: root
    //color: Qt.rgba(0.2,0.2,0.2,0.6)
    color: Qt.rgba(0,0,0,0.8)

    property string mode: "right"

    anchors {
        left: mode === "left" ? parent.left : undefined
        right: mode === "right" ? parent.right : undefined
        top: parent.top
        bottom: parent.bottom
    }

    MouseArea {
        anchors.fill: parent
        onClicked: { expanded = false}
    }

    width: units.gu(35)
    anchors.leftMargin: expanded ? 0 : -width
    anchors.rightMargin: expanded ? 0 : -width

    Behavior on anchors.leftMargin {
        UbuntuNumberAnimation {}
    }

    Behavior on anchors.rightMargin {
        UbuntuNumberAnimation {}
    }

    Flickable {
        id: contents
        anchors {
                top:parent.top
                left: parent.left
                right: parent.right
                bottom: parent.bottom
                rightMargin: mode === "left" ? 1 : 0
                leftMargin: mode === "right" ? 1 : 0
            }
        contentHeight:sideMenu.childrenRect.height



        Column {
            id: sideMenu
            anchors.fill: parent

            CustomListItem{
                //name: ""
                icon: "close"
                iconColor: UbuntuColors.ash
                iconWidth: density <= 10 ? 36 : 96
                iconHeight: iconWidth
                dividerColor: UbuntuColors.ash
                progressSymbol: false
                height: units.gu(6) + divider.height
                onClicked: {
                    expanded = !expanded
                }
            }

            CustomListItem {
                name: '<font color="white">'+ i18n.tr("All") +'</font>'
                iconSource: "../images/dino/icons/all.png"
                iconWidth: density <= 10 ? 36 : 96
                iconHeight: iconWidth
                dividerColor: UbuntuColors.ash
                progressSymbol: false
                onClicked: {
                    collection = DinosaursData.dinosaurs
                    expanded = !expanded
                }
            }

            CustomListItem {
                name: '<font color="white">'+ i18n.tr("Carnivore") +'</font>'
                iconSource: "../images/dino/icons/carnivore.png"
                iconWidth: density <= 10 ? 36 : 96
                iconHeight: iconWidth
                dividerColor: UbuntuColors.ash
                progressSymbol: false
                onClicked: {
                    collection = DinosaursData.dinosaurs
                    collection = collection.filter(function(obj){
                        var diet = i18n.tr("Carnivore")
                        return obj.diet === diet
                    })
                    expanded = !expanded
                }
            }

            CustomListItem {
                name: '<font color="white">'+ i18n.tr("Herbivore") +'</font>'
                iconSource: "../images/dino/icons/herbivore.png"
                iconWidth: density <= 10 ? 36 : 96
                iconHeight: iconWidth
                dividerColor: UbuntuColors.ash
                progressSymbol: false
                onClicked: {
                    collection = DinosaursData.dinosaurs
                    collection = collection.filter(function(obj){
                        var diet = i18n.tr("Herbivore")
                        return obj.diet === diet
                    })
                    expanded = !expanded
                }
            }

            CustomListItem {
                name: '<font color="white">'+ i18n.tr("Omnivore") +'</font>'
                iconSource: "../images/dino/icons/omnivore.png"
                iconWidth: density <= 10 ? 36 : 96
                iconHeight: iconWidth
                dividerColor: UbuntuColors.ash
                progressSymbol: false
                onClicked: {
                    collection = DinosaursData.dinosaurs
                    collection = collection.filter(function(obj){
                        var diet = i18n.tr("Omnivore")
                        return obj.diet === diet
                    })
                    expanded = !expanded
                }
            }

            CustomListItem {
                name: '<font color="white">'+ i18n.tr("Biped") +'</font>'
                iconSource: "../images/dino/icons/biped.png"
                iconWidth: density <= 10 ? 36 : 96
                iconHeight: iconWidth
                dividerColor: UbuntuColors.ash
                progressSymbol: false
                onClicked: {
                    collection = DinosaursData.dinosaurs
                    collection = collection.filter(function(obj){
                        var locomotion = i18n.tr("Biped")
                        return obj.locomotion === locomotion
                    })
                    expanded = !expanded
                }
            }

            CustomListItem {
                name: '<font color="white">'+ i18n.tr("Quadruped") +'</font>'
                iconSource: "../images/dino/icons/quadruped.png"
                iconWidth: density <= 10 ? 36 : 96
                iconHeight: iconWidth
                dividerColor: UbuntuColors.ash
                progressSymbol: false
                onClicked: {
                    collection = DinosaursData.dinosaurs
                    collection = collection.filter(function(obj){
                        var locomotion = i18n.tr("Quadruped")
                        return obj.locomotion === locomotion
                    })
                    expanded = !expanded
                }
            }

            CustomListItem {
                name: '<font color="white">'+ i18n.tr("Marine") +'</font>'
                iconSource: "../images/dino/icons/marine.png"
                iconWidth: density <= 10 ? 36 : 96
                iconHeight: iconWidth
                dividerColor: UbuntuColors.ash
                progressSymbol: false
                onClicked: {
                    collection = DinosaursData.dinosaurs
                    collection = collection.filter(function(obj){
                        var locomotion = i18n.tr("Marine")
                        return obj.locomotion === locomotion
                    })
                    expanded = !expanded
                }
            }

            CustomListItem {
                name: '<font color="white">'+ i18n.tr("Triassic") +'</font>'
                iconSource: "../images/dino/icons/period.png"
                iconWidth: density <= 10 ? 36 : 96
                iconHeight: iconWidth
                dividerColor: UbuntuColors.ash
                progressSymbol: false
                onClicked: {
                    collection = DinosaursData.dinosaurs
                    collection = collection.filter(function(obj){
                        var period = i18n.tr("Triassic")
                        return obj.period === period
                    })
                    expanded = !expanded
                }
            }

            CustomListItem {
                name: '<font color="white">'+ i18n.tr("Jurassic") +'</font>'
                iconSource: "../images/dino/icons/period.png"
                iconWidth: density <= 10 ? 36 : 96
                iconHeight: iconWidth
                dividerColor: UbuntuColors.ash
                progressSymbol: false
                onClicked: {
                    collection = DinosaursData.dinosaurs
                    collection = collection.filter(function(obj){
                        var period = i18n.tr("Jurassic")
                        return obj.period === period
                    })
                    expanded = !expanded
                }
            }

            CustomListItem {
                name: '<font color="white">'+ i18n.tr("Cretaceous") +'</font>'
                iconSource: "../images/dino/icons/period.png"
                iconWidth: density <= 10 ? 36 : 96
                iconHeight: iconWidth
                dividerColor: UbuntuColors.ash
                progressSymbol: false
                onClicked: {
                    collection = DinosaursData.dinosaurs
                    collection = collection.filter(function(obj){
                        var period = i18n.tr("Cretaceous")
                        return obj.period === period
                    })
                    expanded = !expanded
                }
            }

        }

    }

}
