/*
 * Copyright (C) 2018 Wproject - Aitzol Berasategi
 *
 * This file is part of MacBank
 *
 * MacBank is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MacBank is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.4
import QtQuick.Layouts 1.1
import QtQuick.Controls 2.1
import QtQuick.Window 2.2
import Ubuntu.Components 1.3
import Ubuntu.Components.ListItems 1.3 as ListItem
import "components"

Page {
    id: aboutPage
    visible: false

    header: PageHeader {
        id: aboutHeader
        flickable: aboutContainer
        title: i18n.tr(headerTitle)
        StyleHints {
            foregroundColor: "#FFFFFF"
            backgroundColor: "#1976D2"
            dividerColor: UbuntuColors.slate
        }

        leadingActionBar.actions: [
            Action {
                visible: true
                iconName: "back"
                text: "Back"
                onTriggered: {
                    pageStack.clear(page3)
                    pageStack.push(page1)
                    headerTitle = "Dino"
                }
            }
        ]
        visible: true
    }

    Flickable {
        id: aboutContainer

        anchors.fill: parent
        contentHeight: contentCol.height + units.gu(2)
        anchors.top: aboutHeader.bottom

        Column {
            id: contentCol

            anchors.topMargin: units.gu(2)
            anchors.top: parent.top
            width: parent.width

            Image {
                id: logoInfo
                source: "../assets/logo.svg"
                sourceSize.width: density <= 10 ? 128 : 350
                sourceSize.height: density <= 10 ? 128 : 350
                anchors.horizontalCenter: parent.horizontalCenter
                verticalAlignment: Image.AlignTop
                fillMode:Image.PreserveAspectFit
            }

            ListItem.Empty{

                highlightWhenPressed: false
                height: aboutTextLayout.height + (divider.visible ? divider.height : 0)
                ListItemLayout {
                    id: aboutTextLayout

                    Label{
                        text: i18n.tr("Dino is a simple dinosaur collection. Most of the information has been collected from the <a href='http://dtrain.wikia.com/wiki/Dinosaur_Train_Wiki'> Fandom </a> community wiki, which is under <a href='http://creativecommons.org/licenses/by-sa/3.0/'>CC-BY-SA</a> license. The main icon is based on a free public domain vector clipart from <a href='http://www.clker.com/clipart-pink-baby-dinosaur.html'>clker.com</a>")
                        font.pointSize: units.dp(10)
                        anchors.leftMargin: units.gu(2)
                        anchors.left: parent.left
                        anchors.rightMargin: units.gu(2)
                        anchors.right: parent.right
                        horizontalAlignment : Text.AlignJustify
                        wrapMode: Text.WordWrap
                        onLinkActivated: Qt.openUrlExternally(link)
                    }

                }
            }

            CustomListItem {
                name: i18n.tr("Version")
                content: version
            }

            CustomListItem {
                name: i18n.tr("License")
                content: "GPL-3"
            }

            CustomListItem {
                name: i18n.tr("Source code")
                progressSymbol: true
                onClicked: Qt.openUrlExternally("https://gitlab.com/aitzol76/dino")
            }

            CustomListItem {
                name: i18n.tr("More Apps")
                progressSymbol: true
                onClicked: Qt.openUrlExternally("https://open-store.io/?sort=relevance&search=author%3AWproject")
            }

        }

        ScrollBar.vertical: ScrollBar {
            width: 10
            parent: aboutContainer.parent
            anchors.top: aboutContainer.top
            anchors.right: aboutContainer.right
            anchors.bottom: aboutContainer.bottom
            policy: ScrollBar.AsNeeded
        }

    }

}
