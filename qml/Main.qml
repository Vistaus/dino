import QtQuick 2.4
import QtQuick.Window 2.2
import Qt.labs.settings 1.0
import Ubuntu.Components 1.3
import Ubuntu.Components.ListItems 1.3 as ListItem
import "components"
import Dino 1.0 // Dino plugin
import "dino.js" as DinosaursData

MainView {
    objectName: "mainView"
    applicationName: "dino.aitzol76"

    anchorToKeyboard: true

    width: units.gu(75)
    height: units.gu(100)

    property string headerTitle: "Dino"
    property var version: "0.7"
    property real selectedIndex
    property real items
    property string imgFolder :"dinosaurs"
    property var collection: DinosaursData.dinosaurs
    property bool expanded: false

    property var settings: Settings {
        property bool firstRun: true
    }
    property var orientation : width>height ? 2:1
    property real density: Screen.pixelDensity
    property bool deviceType: Dino.isTouch //from C++ Dino plugin

    property var mainHeader : MainHeader{}
    property var searchHeader: SearchHeader{}

    PageStack {
        id: pageStack
        Component.onCompleted: {
            console.log("touchScreen:" + deviceType)
            // Show the welcome wizard only when running the app for the first time
            if (settings.firstRun) {
                console.log("[LOG]: Detecting first time run by user. Starting welcome wizard.")

                push(Qt.resolvedUrl("intro/Intro.qml"))
            } else {
                push(page1)
            }
        }

        //window orientation
        Timer {
            id: timer
        }
        function delay(delayTime, cb) {
            timer.interval = delayTime;
            timer.repeat = false;
            timer.triggered.connect(cb);
            timer.start();
        }
        onWidthChanged: {
            delay(100, function() {
                var orientation = width>height ? 2:1
                console.log(density)

                return orientation
            })

        }

        DinoListview {
          id: page1
        }

        DinoView {
          id: page2
        }

        AboutPage {
          id:page3
        }

        SearchFieldComponent {
          id: searchFieldComponent
        }

    }

    Sidebar{
        id:sidebar
    }

    Component.onCompleted: Dino.main()
}
