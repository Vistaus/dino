import QtQuick 2.4
import QtQuick.Window 2.2
import Ubuntu.Components 1.3
import Ubuntu.Components.ListItems 1.3 as ListItem
import QtGraphicalEffects 1.0



Page {
    id: dinoViewPage
    visible: false
    header: PageHeader {
        id: dinoViewHeader
        title: i18n.tr(headerTitle)
        StyleHints {
            foregroundColor: "#FFFFFF"
            backgroundColor: "#1976D2"
            dividerColor: UbuntuColors.slate
        }

        leadingActionBar.actions: [
            Action {
                visible: true
                iconName: "back"
                text: "Back"
                onTriggered: {
                    pageStack.clear(page2)
                    pageStack.push(page1)
                    page1.header === searchHeader ? searchHeader.field.item.forceActiveFocus() : null //force focus on the search field
                    headerTitle = "Dino"
                }
            }
        ]
        visible: true
    }

    function imgSource(){
            var imgId = collection[selectedIndex].id;
            var source = "images/"+imgFolder+"/img"+imgId+".png"
            //console.log("screen="+orientation)
            //console.log("width:"+width)
            //console.log("height:"+height)
            //console.log(orientation)
            //console.log("width:"+width)
            //console.log("height:"+height)
            console.log(density)
            return source;
    }



    Flickable {
        id: dinoContainer
        width: parent.width; height: parent.height
        anchors.top: dinoViewHeader.bottom
        anchors.bottom: parent.bottom

        ScrollView {
            id: dataCont
            width: orientation === 1 ? parent.width : parent.width/2
            height: orientation === 1 ? parent.height/2 : parent.height
            anchors.left : orientation === 1 ? parent.left : parent.left

            Column {
                width: orientation === 1 ? dinoContainer.width : dinoContainer.width/2

                ListItem.Standard {
                    text: "<b>"+  i18n.tr("Meaning")+"</b>: "  + collection[selectedIndex].meaning
                }
                ListItem.Standard {
                    text: "<b>"+ i18n.tr("Period")+"</b>: " + collection[selectedIndex].period
                }
                ListItem.Standard {
                    text: "<b>"+ i18n.tr("Diet")+"</b>: " + collection[selectedIndex].diet
                }
                ListItem.Standard {
                    text: "<b>"+ i18n.tr("Height")+"</b>: " + collection[selectedIndex].height
                }
                ListItem.Standard {
                    text: "<b>"+ i18n.tr("Weight")+"</b>: " + collection[selectedIndex].weight
                }
                ListItem.Standard {
                    text: "<b>"+ i18n.tr("Locomotion")+"</b>: " + collection[selectedIndex].locomotion
                }
                ListItem.Standard {
                    text: "<b>"+ i18n.tr("Species")+"</b>: " + collection[selectedIndex].species
                }
                ListItem.Standard {
                    text: "<b>"+ i18n.tr("Family")+"</b>: " + collection[selectedIndex].family
                }
                ListItem.Standard {
                    text: "<b>"+ i18n.tr("Home")+"</b>: " + collection[selectedIndex].home
                }
                ListItem.Caption {
                    id:description
                    text: "<b>"+ i18n.tr("Description")+"</b>: " + collection[selectedIndex].description
                }

                ListItem.ThinDivider{
                    height: 2
                }
            }

        }

        Rectangle{
            id: imgCont
            width: orientation === 2 ? parent.width/2 : parent.width
            height: orientation === 2 ? parent.height/2 : parent.height*2
            anchors.top: orientation === 1 ? dataCont.bottom : parent.top
            anchors.right:parent.right
            anchors.bottom:parent.bottom

            gradient: Gradient
            {
                GradientStop { position: 0.5; color: "#ffffff" }
                GradientStop { position: 1.0; color: "#ebebe0" }
            }

            Rectangle {
                id: verticalDivider
                border.width: .5
                height: parent.height
                width: 2
                anchors.left: parent.left
                border.color: "#cccccc"
                visible: orientation === 2 ? true : false
            }

            Image {
                id:dinoImage
                anchors.fill: parent
                anchors.margins: parent.width/20
                anchors.verticalCenter: parent.verticalCenter
                //anchors.horizontalCenter: parent.horizontalCenter
                anchors.horizontalCenter: orientation === 1 ? parent.horizontalCenter : imgCont.horizontalCenter
                horizontalAlignment: Image.AlignHCenter
                verticalAlignment: Image.AlignVCenter
                source:imgSource()
                fillMode: Image.PreserveAspectFit
                smooth: true
                visible: false

            }
            DropShadow {
                anchors.fill: dinoImage
                horizontalOffset: 0
                verticalOffset: 20
                radius: 8.0
                samples: 17
                color: "#999966"
                source: dinoImage
            }

        }

        //---------------Swipe based on MouseArea for desktop -------------------
        MouseArea{
            //enabled if device doesn't have a touch screen
            enabled: deviceType === true ? false:true
            anchors.fill: parent
            property point origin
            property point destination
            //property bool ready: false
            signal move(int x, int y)
            signal swipe(string direction)
            onPressed: {
                drag.axis = Drag.XAxis
                origin = Qt.point(mouse.x, mouse.y)
                console.log(origin)
            }
            onReleased: {
                destination =  Qt.point(mouse.x, mouse.y)
                console.log(destination.x)
                if(origin.x != destination.x){
                    if(origin.x < destination.x){

                        if(selectedIndex != 0){
                            selectedIndex = selectedIndex - 1
                            console.log("SW-right")
                            console.log(selectedIndex)
                            headerTitle = collection[selectedIndex].name
                            pageStack.push(page2)
                        }

                    }else{

                        if(selectedIndex < items){
                            selectedIndex = selectedIndex + 1
                            console.log("SW-left")
                            console.log(selectedIndex)
                            headerTitle = collection[selectedIndex].name
                            pageStack.push(page2)
                        }

                    }

                }

            }
        }
        //-----------------Swipe based on SwipeArea for touchscreen devices ----------------------
        SwipeArea {
            //enabled if device has a touch screen
            enabled: deviceType === true ? true:false
            anchors.fill: parent
            direction: SwipeArea.Leftwards
            height: units.gu(50)
            //immediateRecognition : true
            onDraggingChanged:  {
                if ( dragging ) {
                    if(selectedIndex < items){
                        selectedIndex = selectedIndex + 1
                        console.log("SW-left")
                        console.log(selectedIndex)
                        headerTitle = collection[selectedIndex].name
                        pageStack.push(page2)
                    }
                }
            }
        }
        SwipeArea {
            anchors.fill: parent
            direction: SwipeArea.Rightwards
            height: units.gu(50)
            //immediateRecognition : true
            onDraggingChanged:  {
                if ( dragging ) {
                    if(selectedIndex != 0){
                        selectedIndex = selectedIndex - 1
                        console.log("SW-right")
                        console.log(selectedIndex)
                        headerTitle = collection[selectedIndex].name
                        pageStack.push(page2)
                    }
                }
            }
        }

    //--------------------------

    }

}
