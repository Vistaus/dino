#include <QDebug>
#include <QQuickView>
#include <QQuickItem>
#include <QTouchDevice>

#include "dino.h"

Dino::Dino() {

}

int Dino::getDevice() {

  bool isTouch = false;
  foreach (const QTouchDevice *dev, QTouchDevice::devices())
      if (dev->type() == QTouchDevice::TouchScreen) {
          isTouch = true;
          break;
      }

  return isTouch;
}
