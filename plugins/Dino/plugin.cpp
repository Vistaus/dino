#include <QtQml>
#include <QtQml/QQmlContext>

#include "plugin.h"
#include "dino.h"

void DinoPlugin::registerTypes(const char *uri) {
    //@uri Dino
    qmlRegisterSingletonType<Dino>(uri, 1, 0, "Dino", [](QQmlEngine*, QJSEngine*) -> QObject* { return new Dino; });
}
