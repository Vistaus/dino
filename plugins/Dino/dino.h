#ifndef DINO_H
#define DINO_H

#include <QObject>

class Dino: public QObject {
    Q_OBJECT
    Q_PROPERTY(bool isTouch MEMBER isTouch NOTIFY main)

public:
    Dino();
    ~Dino() = default;

    Q_INVOKABLE int getDevice();

signals:
    int main();

private:

  bool isTouch = Dino::getDevice();

};

#endif
